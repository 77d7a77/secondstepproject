const burgerIcon = document.querySelector(".header-body__burger");
const burgerMenu = document.querySelector(".main-header__menu");
(function clickBurgerMenu() {
    burgerIcon.addEventListener("click", () => {
        burgerIcon.classList.toggle("active")
        if (burgerIcon.classList.contains("active")) {
            document.querySelector(".main-header__menu").classList.add("active")
        } else {
            document.querySelector(".main-header__menu").classList.remove("active")
        }
    })
}());
window.addEventListener("click", e => {
    const target = e.target
    if (!target.closest(".header-body__burger") && !target.closest(".main-header__menu")) {
        burgerMenu.classList.remove("active");
        burgerIcon.classList.remove("active");
    }
})
document.querySelector(".prising-list").addEventListener("click", e => {
    if (e.target.classList.contains("prising-list__card")) {
        document.querySelector(".prising-list .prising-list__active-card").classList.remove("prising-list__active-card");
        e.target.classList.add("prising-list__active-card");
    }
})