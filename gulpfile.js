const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require("gulp-autoprefixer");
const uglify = require('gulp-uglify');
const deleteFile = require('delete');
const sass = require("gulp-sass")(require('sass'));
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const jsmin = require('gulp-jsmin');
const browserSync = require('browser-sync').create();
gulp.task("browserSync", function (done) {
    browserSync.init({
        server: "dist/"
    });
    gulp.watch(["src/styles/**/**/*.scss", "src/html/*.html", "src/js/**/**/*.js", "src/img/**/"],
        gulp.series("build")
    )
    gulp.watch("scr/html/*.html").on('change', () => {
        browserSync.reload();
        done();
    });
    done();
});
//так и не смог натроить плагин BrowserSync!!!
gulp.task("minify", () => {
    return gulp.src("src/html/*.html")
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest("dist"));
})
gulp.task("build-css", function (done) {
    gulp.src("src/styles/style.scss")
        .pipe(autoprefixer({
            browsers: ["last 2 version"],
            cascade: false
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(cssmin())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/style"))
        .pipe(browserSync.stream());


    done();
})
gulp.task("build-js", function (done) {
    return gulp.src("src/js/*.js")
        .pipe(jsmin())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(gulp.dest("dist/js"));
})

gulp.task("buildImg", function () {
    return gulp.src("src/img/**")
        .pipe(gulp.dest("dist/img/"));
});
gulp.task("deleteFiles", function () {
    return deleteFile("dist/")
})
gulp.task("build", gulp.series("deleteFiles", gulp.parallel("minify", "build-js", "build-css", "buildImg")))
gulp.task("dev", gulp.series("build", "browserSync"))//в терменале что бы сразу отслеживать изминениея!(gulp dev как мы делали gulp build)
//;